using System;
using System.Runtime.CompilerServices;

namespace YPScraper.Model
{
	internal class SearchQuery
	{
		public string Keyword
		{
			get;
			set;
		}

		public string Location
		{
			get;
			set;
		}

		public string Site
		{
			get;
			set;
		}

		public SearchQuery()
		{
		}
	}
}