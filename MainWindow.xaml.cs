using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using YPScraper.Business;
using YPScraper.Model;

namespace YPScraper
{
	public partial class MainWindow : Window
	{
		private static List<YPScraper.Business.SearchResult> searchResults;

		private static int numSearchResults;

		private static int numSearchResultPages;

		private List<string> sites;

		private List<string> filters;

		private SearchQuery searchQuery;

		private List<BackgroundWorker> workers;

		public MainWindow()
		{
			this.InitializeComponent();
			MainWindow.searchResults = new List<YPScraper.Business.SearchResult>();
			MainWindow.numSearchResults = 0;
			this.sites = new List<string>()
			{
				"Yellow Pages"
			};
			List<string> strs = new List<string>()
			{
				"Company",
				"Address",
				"Phone",
				"City",
				"State",
				"Zip",
				"Website",
				"Email",
				"Keyword1",
				"Keyword2",
				"Keyword3",
				"Keyword4"
			};
			this.filters = strs;
			this.SiteComboBox.ItemsSource = this.sites;
			this.FilterComboBox.ItemsSource = this.filters;
			this.workers = new List<BackgroundWorker>();
			this.searchQuery = new SearchQuery();
			this.KeywordTextBox.Focus();
			this.DataGrid.ItemsSource = MainWindow.searchResults;
		}

		private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
		{
			int index = e.Row.GetIndex() + 1;
			e.Row.Header = index.ToString();
		}

		private static DataTable DataViewAsDataTable(DataView dv)
		{
			DataTable dataTable = dv.Table.Clone();
			foreach (DataRowView dataRowView in dv)
			{
				dataTable.ImportRow(dataRowView.Row);
			}
			return dataTable;
		}

		private void ExitButton_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void ExportButton_Click(object sender, RoutedEventArgs e)
		{
			this.DataGrid.SelectAllCells();
			this.DataGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
			ApplicationCommands.Copy.Execute(null, this.DataGrid);
			this.DataGrid.UnselectAllCells();
			string data = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
			Clipboard.Clear();
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string str = string.Concat(this.searchQuery.Keyword.ToLower().Trim().Replace(" ", "_"), "_", this.searchQuery.Location.Trim().Replace(",", "-").Replace(" ", "_"), ".csv");
			StreamWriter streamWriter = new StreamWriter(string.Concat(folderPath, "\\", str));
			streamWriter.WriteLine(data);
			streamWriter.Close();
			MessageBox.Show(string.Concat("Exported ", str, " to Desktop successfully."), "Success");
		}

		private void FilterButton_Click(object sender, RoutedEventArgs e)
		{
			string str = this.FilterComboBox.SelectedItem.ToString();
			if (str == "Company")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Company == null ? false : s.Company != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Address")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Address == null ? false : s.Address != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Phone")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Phone == null ? false : s.Phone != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "City")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.City == null ? false : s.City != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "State")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.State == null ? false : s.State != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Zip")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Zip == null ? false : s.Zip != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Website")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Website == null ? false : s.Website != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Email")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Email == null ? false : s.Email != "")
					select s).ToList<YPScraper.Business.SearchResult>();
				MainWindow.searchResults = (
					from sr in MainWindow.searchResults
					group sr by sr.Email into grp
					select grp.First<YPScraper.Business.SearchResult>()).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Keyword1")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Keyword1 == null ? false : s.Keyword1 != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Keyword2")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Keyword2 == null ? false : s.Keyword2 != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Keyword3")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Keyword3 == null ? false : s.Keyword3 != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			else if (str == "Keyword4")
			{
				MainWindow.searchResults = (
					from s in MainWindow.searchResults
					where (s.Keyword4 == null ? false : s.Keyword4 != "")
					select s).ToList<YPScraper.Business.SearchResult>();
			}
			this.DataGrid.ItemsSource = MainWindow.searchResults;
			this.DataGrid.Items.Refresh();
			Label resultsLabel = this.ResultsLabel;
			object[] count = new object[] { "Results ", MainWindow.searchResults.Count, "/", MainWindow.searchResults.Count };
			resultsLabel.Content = string.Concat(count);
			this.LoadingAnimationGrid.Visibility = System.Windows.Visibility.Hidden;
		}

		private void FindCompaniesThread(object sender, DoWorkEventArgs e)
		{
			int argument = (int)e.Argument;
			ScraperYP scraperYP = new ScraperYP();
			object[] location = new object[] { "http://www.yellowpages.com/", this.searchQuery.Location, "/", this.searchQuery.Keyword, "?page=", argument };
			scraperYP.LoadURL(string.Concat(location));
			MainWindow.searchResults.AddRange(scraperYP.getSearchResultsYP());
		}

		private void FindCompaniesThreadComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((ScraperYP.ErrorMessage == null || !(ScraperYP.ErrorMessage != "") || ScraperYP.ErrorMessage.Contains("timed out") || ScraperYP.ErrorMessage.Contains("underlying") ? false : !ScraperYP.ErrorMessage.Contains("resolved:")))
			{
				ScraperYP.ErrorMessage = "";
			}
			if (!e.Cancelled)
			{
				Label resultsLabel = this.ResultsLabel;
				object[] count = new object[] { "Results ", MainWindow.searchResults.Count, "/", MainWindow.numSearchResults };
				resultsLabel.Content = string.Concat(count);
				this.DataGrid.ItemsSource = MainWindow.searchResults;
				this.DataGrid.Items.Refresh();
				if (MainWindow.searchResults.Count >= MainWindow.numSearchResults)
				{
					this.LoadingAnimationGrid.Visibility = System.Windows.Visibility.Hidden;
				}
			}
		}

		private static bool isZipCode(string strToCheck)
		{
			return ((new Regex("[0-9]*$")).IsMatch(strToCheck) ? false : true);
		}

		private void ResetButton_Click(object sender, RoutedEventArgs e)
		{
			foreach (BackgroundWorker worker in this.workers)
			{
				if (worker.IsBusy)
				{
					worker.CancelAsync();
				}
			}
			this.workers = new List<BackgroundWorker>();
			MainWindow.searchResults = new List<YPScraper.Business.SearchResult>();
			MainWindow.numSearchResults = 0;
			this.searchQuery = new SearchQuery();
			this.DataGrid.ItemsSource = MainWindow.searchResults;
			this.KeywordTextBox.Text = "";
			this.LocationTextBox.Text = "";
			this.ResultsLabel.Content = "Results 0/0";
			this.LoadingAnimationGrid.Visibility = System.Windows.Visibility.Hidden;
			this.KeywordTextBox.Focus();
		}

		private void ScrapeButton_Click(object sender, RoutedEventArgs e)
		{
			this.searchQuery.Keyword = this.KeywordTextBox.Text;
			this.searchQuery.Location = this.LocationTextBox.Text;
			this.searchQuery.Site = this.SiteComboBox.SelectedItem.ToString();
			if ((this.searchQuery.Keyword == "" ? true : this.searchQuery.Location == ""))
			{
				MessageBox.Show("You must enter search criteria.", "Search Criteria Required", MessageBoxButton.OK);
			}
			this.LoadingAnimationGrid.Visibility = System.Windows.Visibility.Visible;
			if (this.searchQuery.Site == "Yellow Pages")
			{
				this.searchQuery.Keyword = this.searchQuery.Keyword.ToLower();
				this.searchQuery.Keyword = this.searchQuery.Keyword.Trim();
				this.searchQuery.Keyword = this.searchQuery.Keyword.Replace(" ", "-");
				if (!MainWindow.isZipCode(this.searchQuery.Location))
				{
					this.searchQuery.Location = this.searchQuery.Location.ToLower();
					this.searchQuery.Location = this.searchQuery.Location.Replace(",", "");
					this.searchQuery.Location = this.searchQuery.Location.Trim();
					this.searchQuery.Location = this.searchQuery.Location.Replace(" ", "-");
				}
				string str = string.Concat("http://www.yellowpages.com/", this.searchQuery.Location, "/", this.searchQuery.Keyword);
				ScraperYP scraperYP = new ScraperYP();
				scraperYP.LoadURL(str);
				int numSearchResultsYP = scraperYP.getNumSearchResultsYP();
				MainWindow.numSearchResults = MainWindow.numSearchResults + numSearchResultsYP;
				MainWindow.numSearchResultPages = numSearchResultsYP / 30;
				if (numSearchResultsYP % 30 != 0)
				{
					MainWindow.numSearchResultPages = MainWindow.numSearchResultPages + 1;
				}
				Label resultsLabel = this.ResultsLabel;
				object[] count = new object[] { "Results ", MainWindow.searchResults.Count, "/", MainWindow.numSearchResults };
				resultsLabel.Content = string.Concat(count);
				for (int i = this.workers.Count; i < MainWindow.numSearchResultPages; i++)
				{
					this.workers.Add(new BackgroundWorker());
					this.workers[this.workers.Count - 1].WorkerSupportsCancellation = true;
					this.workers[this.workers.Count - 1].DoWork += new DoWorkEventHandler(this.FindCompaniesThread);
					this.workers[this.workers.Count - 1].RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.FindCompaniesThreadComplete);
					this.workers[this.workers.Count - 1].RunWorkerAsync(i);
				}
			}
		}

		private void SendToDatabaseButton_Click(object sender, RoutedEventArgs e)
		{
			foreach (YPScraper.Business.SearchResult searchResult in MainWindow.searchResults)
			{
				Database.SaveData(searchResult);
			}
			if (!(Database.ErrorMessage != ""))
			{
				MessageBox.Show("Data sent to database successfully", "Success", MessageBoxButton.OK, MessageBoxImage.Asterisk);
			}
			else
			{
				MessageBox.Show(Database.ErrorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
			}
		}
	}
}